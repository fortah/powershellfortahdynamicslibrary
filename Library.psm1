using namespace System.Collections.Generic
using namespace System.IO

class FortahDynamicsAdmin : FortahDynamicsAdminBase {

    FortahDynamicsAdmin([string] $pPath) : base($pPath) {        
    }

}

class FortahDynamicsAdminBase {

    hidden [string] $path = ''
    [string] getPath() { return $this.path }
    [void] setPath([string] $pValue) { $this.path = $pValue }

    FortahDynamicsAdminBase([string] $pPath) {
        $this.path = $pPath
    }

    [List[FortahDynamicsApplication]] getApplications() {
        [List[FortahDynamicsApplication]] $installedApplications = $this.getInstalledApplications()        
        [List[FortahDynamicsApplication]] $fileApplications = $this.getFileApplications()
        return [FortahDynamicsAdminToolkit]::combineApplications($installedApplications, $fileApplications)                
    }

    hidden [List[FortahDynamicsApplication]] getInstalledApplications() {
        return [List[FortahDynamicsApplication]]::new()
    }
    
    hidden [List[FortahDynamicsApplication]] getFileApplications() {
        return [List[FortahDynamicsApplication]]::new()
    }

}

class FortahDynamicsAdminToolkit {

    static [List[FortahDynamicsApplication]] combineApplications([List[FortahDynamicsApplication]] $pInstalledApplications, [List[FortahDynamicsApplication]] $pFileApplications) {
        [List[FortahDynamicsApplication]] $applications = [List[FortahDynamicsApplication]]::new()
        
        [FortahDynamicsApplication] $installedApplication = $null
        foreach ($installedApplication in $pInstalledApplications) {
            $applications.Add($installedApplication)
        }

        [FortahDynamicsApplication] $fileApplication = $null
        foreach ($fileApplication in $pFileApplications) {
            [FortahDynamicsApplication] $application = [FortahDynamicsAdminToolkit]::findApplication($applications)
            if ($null -ne $application) {
                [FortahDynamicsAdminToolkit]::mergeApplications($application, $fileApplication)
            } else {
                $applications.Add($fileApplication)
            }
        }

        return $applications
    }

    static [FortahDynamicsApplication] findApplication([string] $pIdToFind, [FortahVersion] $pVersionToFind, [List[FortahDynamicsApplication]] $pApplications) {        
        [FortahDynamicsApplication] $application = $null
        [List[FortahDynamicsApplication]] $applications = @($pApplications.Extensions | Where-Object { (($_.getId() -eq $pIdToFind) -and ($_.getVersion().isEqual($pVersionToFind))) })  
        if ($applications.Count -gt 0) {
            $application = $applications[0]
        }
        return $application
    }

    static [void] mergeApplications([FortahDynamicsApplication] $pApplication, [FortahDynamicsApplication] $pFileApplication) {
        $pApplication.setFile($true)
        $pApplication.setFilePath($pFileApplication.getFilePath())
    }

}

class FortahDynamicsStubAdmin : FortahDynamicsAdminBase {

    FortahDynamicsStubAdmin([string] $pPath) : base($pPath) {        
    }

}

class FortahDynamicsEnvironment : FortahDynamicsEnvironmentBase {

    FortahDynamicsEnvironment([string] $pRootPath) : base($pRootPath) {        
    }

    hidden [string] detectRootPath() {        
        #TODO >>> Not implemented yet
        return ''
    }

    hidden [bool] loadLibraryFromPath([string] $pLibraryPath) {
        #TODO >>> Not implemented yet
        return $false
    }
}

class FortahDynamicsEnvironmentBase {
    
    hidden [string] $rootPath = ''
    [string] getRootPath() { return $this.rootPath }
    [void] setRootPath([string] $pValue) { $this.rootPath = $pValue }

    FortahDynamicsEnvironmentBase([string] $pRootPath) {
        $this.rootPath = $pRootPath
    }

    [void] loadModules([List[FortahDynamicsModule]] $pModules) {
        $this.makeSureRootPathIsNotEmpty()
        [FortahDynamicsModule] $module = $null
        foreach ($module in $pModules) {
            [bool] $loaded = $false
            [string] $library = ''
            foreach ($library in $module.getLibraries()) {
                if ($this.loadLibrary($library)) {
                    $loaded = $true
                    break
                }
            }
            if ( -not ($loaded)) {
                throw "Cannot load $($module.getName()) module"
            }
        }        
    }

    hidden [void] makeSureRootPathIsNotEmpty() {
        if ($this.rootPath -eq '') {
            $this.rootPath = $this.detectRootPath()
        }        
        if ($this.rootPath -eq '') {
            throw "Cannot determine root path of Dynamics 365 Business Central"
        }
    }

    hidden [string] detectRootPath() {
        return ''        
    }

    hidden [bool] loadLibrary([string] $pLibrary) {
        [bool] $loaded = $false
        [string] $libraryPath = $this.findLibrary($pLibrary, $this.rootPath)
        if ($libraryPath -ne '') {
            $loaded = $this.loadLibraryFromPath($libraryPath)
        }
        return $loaded        
    }

    hidden [bool] loadLibraryFromPath([string] $pLibraryPath) {
        return $false
    }

    hidden [string] findLibrary([string] $pLibrary, [string] $pPath) {
        [string] $libraryPath = ''
        [List[string]] $files = [Directory]::GetFiles($pPath, $pLibrary)
        if ($files.Count -gt 0) {
            $libraryPath = $files[0]
        } else {
            [List[string]] $directories = [Directory]::GetDirectories($pPath)
            [string] $directory = ''
            foreach ($directory in $directories) {
                $libraryPath = $this.findLibrary($pLibrary, [Path]::Combine($pPath, $directory))
                if ($libraryPath -ne '') {
                    break
                }
            }
        }
        return $libraryPath
    }

}

class FortahDynamicsModule {

    hidden [string] $name = ''
    [string] getName() { return $this.name }

    hidden [List[string]] $libraries = [List[string]]::new()
    [List[string]] getLibraries() { return $this.libraries }

    FortahDynamicsModule([string] $pName, [List[string]] $pLibraries) {
        $this.name = $pName
        $this.libraries = $pLibraries        
    }

}

class FortahDynamicsStubEnvironment : FortahDynamicsEnvironmentBase {

    FortahDynamicsStubEnvironment([string] $pRootPath) : base($pRootPath) {        
    }

    hidden [string] detectRootPath() {        
        return '/home/Temp'
    }

    hidden [bool] loadLibraryFromPath([string] $pLibraryPath) {
        return $true
    }
}

class FortahDynamicsApplication : FortahDynamicsApplicationBase {

    hidden [string] $brief = ''
    [string] getBrief() { return $this.brief }
    [void] setBrief([string] $pValue) { $this.brief = $pValue }

    hidden [string] $description = ''
    [string] getDescription() { return $this.description }
    [void] setDescription([string] $pValue) { $this.description = $pValue }

    hidden [FortahVersion] $dataVersion = [FortahVersion]::new()
    [FortahVersion] getDataVersion() { return $this.dataVersion }
    [void] setDataVersion([FortahVersion] $pValue) { $this.dataVersion = $pValue }

    hidden [FortahDynamicsScope] $scope = [FortahDynamicsScope]::null
    [FortahDynamicsScope] getScope() { return $this.scope }
    [void] setScope([FortahDynamicsScope] $pValue) { $this.scope = $pValue }

    hidden [bool] $published = $false
    [bool] isPublished() { return $this.published }
    [void] setPublished([bool] $pValue) { $this.published = $pValue }

    hidden [bool] $synchronised = $false
    [bool] isSynchronised() { return $this.synchronised }
    [void] setSynchronised([bool] $pValue) { $this.synchronised = $pValue }

    hidden [bool] $upgraded = $false
    [bool] isUpgraded() { return $this.upgraded }
    [void] setUpgraded([bool] $pValue) { $this.upgraded = $pValue }

    hidden [bool] $installed = $false
    [bool] isInstalled() { return $this.installed }
    [void] setInstalled([bool] $pValue) { $this.installed = $pValue }

    hidden [bool] $file = $false
    [bool] isFile() { return $this.file }
    [void] setFile([bool] $pValue) { $this.file = $pValue }

    hidden [string] $filePath = ''
    [string] getFilePath() { return $this.filePath }
    [void] setFilePath([string] $pValue) { $this.filePath = $pValue }

    [List[FortahDynamicsDependency]] $dependencies = [List[FortahDynamicsDependency]]::new()
    [string] getDependencies() { return $this.dependencies }
    
    FortahDynamicsApplication() {        
    }

}

class FortahDynamicsApplicationBase {
    
    hidden [string] $id = ''
    [string] getId() { return $this.id }
    [void] setId([string] $pValue) { $this.id = $pValue }

    hidden [string] $name = ''
    [string] getName() { return $this.name }
    [void] setName([string] $pValue) { $this.name = $pValue }

    hidden [string] $publisher = ''
    [string] getPublisher() { return $this.publisher }
    [void] setPublisher([string] $pValue) { $this.publisher = $pValue }

    hidden [FortahVersion] $version = [FortahVersion]::new()
    [FortahVersion] getVersion() { return $this.version }
    [void] setVersion([FortahVersion] $pValue) { $this.version = $pValue }

    FortahDynamicsApplicationBase() {        
    }
    
}

class FortahDynamicsDependency : FortahDynamicsApplicationBase {
    
    FortahDynamicsDependency() {        
    }

}

enum FortahDynamicsScope {
    null = 0
    global = 1
    tenant = 2
}

class FortahDynamicsScopeEx {

    static [string] format([FortahDynamicsScope] $pValue) {
        [string] $text = ''
        switch ($pValue) {
            ([FortahDynamicsScope]::global) {
                $text = 'global'
            }
            ([FortahDynamicsScope]::tenant) {
                $text = 'tenant'
            }
        }
        return $text
    }

    static [FortahDynamicsScope] parse([string] $pText) {
        [FortahDynamicsScope] $value = [FortahDynamicsScope]::global
        [string] $textToParse = $pText.ToLower().Trim()
        switch ($textToParse) {
            ('global') {
                $value = [FortahDynamicsScope]::global
            }
            ('tenant') {
                $value = [FortahDynamicsScope]::tenant
            }
        }
        return $value 
    }

}

class FortahDynamicsManager {
    
    FortahDynamicsManager() : base() {        
    }

}

