using namespace System.Collections.Generic

using module '.\FortahDynamicsAdminToolkit.psm1'
using module '..\General\FortahDynamicsApplication.psm1'

class FortahDynamicsAdminBase {

    hidden [string] $path = ''
    [string] getPath() { return $this.path }
    [void] setPath([string] $pValue) { $this.path = $pValue }

    FortahDynamicsAdminBase([string] $pPath) {
        $this.path = $pPath
    }

    [List[FortahDynamicsApplication]] getApplications() {
        [List[FortahDynamicsApplication]] $installedApplications = $this.getInstalledApplications()        
        [List[FortahDynamicsApplication]] $fileApplications = $this.getFileApplications()
        return [FortahDynamicsAdminToolkit]::combineApplications($installedApplications, $fileApplications)                
    }

    hidden [List[FortahDynamicsApplication]] getInstalledApplications() {
        return [List[FortahDynamicsApplication]]::new()
    }
    
    hidden [List[FortahDynamicsApplication]] getFileApplications() {
        return [List[FortahDynamicsApplication]]::new()
    }

}