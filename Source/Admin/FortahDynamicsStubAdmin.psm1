using namespace System.Collections.Generic
using namespace System.IO
using namespace System.IO.Compression

using module '.\FortahDynamicsAdminBase.psm1'
using module '..\Adapters\FortahDynamicsApplicationFileAdapter.psm1'
using module '..\General\FortahDynamicsApplication.psm1'

class FortahDynamicsStubAdmin : FortahDynamicsAdminBase {

    FortahDynamicsStubAdmin([string] $pPath) : base($pPath) {        
    }

    hidden [List[FortahDynamicsApplication]] getInstalledApplications() {
        return [List[FortahDynamicsApplication]]::new()
    }
    
    hidden [List[FortahDynamicsApplication]] getFileApplications() {
        [List[FortahDynamicsApplication]] $applications = [List[FortahDynamicsApplication]]::new()
        [List[string]] $files = [Directory]::GetFiles($this.getPath())
        [string] $file = ''
        foreach ($file in $files) {
            [string] $filePath = Path.Combine($this.getPath(), $file);
            [FortahDynamicsApplication] $application = ([FortahDynamicsApplicationFileAdapter]::new($filePath)).read()
            $applications.Add($application)
        }
        return $applications
    }

}