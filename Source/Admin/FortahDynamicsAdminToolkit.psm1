using namespace System.Collections.Generic

using module '..\General\FortahDynamicsApplication.psm1'
using module '..\..\Libraries\FortahGeneralLibrary\Library.psm1'

class FortahDynamicsAdminToolkit {

    static [List[FortahDynamicsApplication]] combineApplications([List[FortahDynamicsApplication]] $pInstalledApplications, [List[FortahDynamicsApplication]] $pFileApplications) {
        [List[FortahDynamicsApplication]] $applications = [List[FortahDynamicsApplication]]::new()
        
        [FortahDynamicsApplication] $installedApplication = $null
        foreach ($installedApplication in $pInstalledApplications) {
            $applications.Add($installedApplication)
        }

        [FortahDynamicsApplication] $fileApplication = $null
        foreach ($fileApplication in $pFileApplications) {
            [FortahDynamicsApplication] $application = [FortahDynamicsAdminToolkit]::findApplication($applications)
            if ($null -ne $application) {
                [FortahDynamicsAdminToolkit]::mergeApplications($application, $fileApplication)
            } else {
                $applications.Add($fileApplication)
            }
        }

        return $applications
    }

    static [FortahDynamicsApplication] findApplication([string] $pIdToFind, [FortahVersion] $pVersionToFind, [List[FortahDynamicsApplication]] $pApplications) {        
        [FortahDynamicsApplication] $application = $null
        [List[FortahDynamicsApplication]] $applications = @($pApplications.Extensions | Where-Object { (($_.getId() -eq $pIdToFind) -and ($_.getVersion().isEqual($pVersionToFind))) })  
        if ($applications.Count -gt 0) {
            $application = $applications[0]
        }
        return $application
    }

    static [void] mergeApplications([FortahDynamicsApplication] $pApplication, [FortahDynamicsApplication] $pFileApplication) {
        $pApplication.setFile($true)
        $pApplication.setFilePath($pFileApplication.getFilePath())
    }



}