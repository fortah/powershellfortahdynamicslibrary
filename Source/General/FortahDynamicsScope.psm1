enum FortahDynamicsScope {
    null = 0
    global = 1
    tenant = 2
}

class FortahDynamicsScopeEx {

    static [string] format([FortahDynamicsScope] $pValue) {
        [string] $text = ''
        switch ($pValue) {
            ([FortahDynamicsScope]::global) {
                $text = 'global'
            }
            ([FortahDynamicsScope]::tenant) {
                $text = 'tenant'
            }
        }
        return $text
    }

    static [FortahDynamicsScope] parse([string] $pText) {
        [FortahDynamicsScope] $value = [FortahDynamicsScope]::global
        [string] $textToParse = $pText.ToLower().Trim()
        switch ($textToParse) {
            ('global') {
                $value = [FortahDynamicsScope]::global
            }
            ('tenant') {
                $value = [FortahDynamicsScope]::tenant
            }
        }
        return $value 
    }

}