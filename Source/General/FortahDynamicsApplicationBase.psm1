using module '..\..\Libraries\FortahGeneralLibrary\Library.psm1'

class FortahDynamicsApplicationBase {
    
    hidden [string] $id = ''
    [string] getId() { return $this.id }
    [void] setId([string] $pValue) { $this.id = $pValue }

    hidden [string] $name = ''
    [string] getName() { return $this.name }
    [void] setName([string] $pValue) { $this.name = $pValue }

    hidden [string] $publisher = ''
    [string] getPublisher() { return $this.publisher }
    [void] setPublisher([string] $pValue) { $this.publisher = $pValue }

    hidden [FortahVersion] $version = [FortahVersion]::new()
    [FortahVersion] getVersion() { return $this.version }
    [void] setVersion([FortahVersion] $pValue) { $this.version = $pValue }

    FortahDynamicsApplicationBase() {        
    }
    
}