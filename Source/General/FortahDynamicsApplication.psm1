using namespace System.Collections.Generic

using module '.\FortahDynamicsApplicationBase.psm1'
using module '.\FortahDynamicsDependency.psm1'
using module '.\FortahDynamicsScope.psm1'

using module '..\..\Libraries\FortahGeneralLibrary\Library.psm1'

class FortahDynamicsApplication : FortahDynamicsApplicationBase {

    hidden [string] $brief = ''
    [string] getBrief() { return $this.brief }
    [void] setBrief([string] $pValue) { $this.brief = $pValue }

    hidden [string] $description = ''
    [string] getDescription() { return $this.description }
    [void] setDescription([string] $pValue) { $this.description = $pValue }

    hidden [FortahVersion] $dataVersion = [FortahVersion]::new()
    [FortahVersion] getDataVersion() { return $this.dataVersion }
    [void] setDataVersion([FortahVersion] $pValue) { $this.dataVersion = $pValue }

    hidden [FortahDynamicsScope] $scope = [FortahDynamicsScope]::null
    [FortahDynamicsScope] getScope() { return $this.scope }
    [void] setScope([FortahDynamicsScope] $pValue) { $this.scope = $pValue }

    hidden [bool] $published = $false
    [bool] isPublished() { return $this.published }
    [void] setPublished([bool] $pValue) { $this.published = $pValue }

    hidden [bool] $synchronised = $false
    [bool] isSynchronised() { return $this.synchronised }
    [void] setSynchronised([bool] $pValue) { $this.synchronised = $pValue }

    hidden [bool] $upgraded = $false
    [bool] isUpgraded() { return $this.upgraded }
    [void] setUpgraded([bool] $pValue) { $this.upgraded = $pValue }

    hidden [bool] $installed = $false
    [bool] isInstalled() { return $this.installed }
    [void] setInstalled([bool] $pValue) { $this.installed = $pValue }

    hidden [bool] $file = $false
    [bool] isFile() { return $this.file }
    [void] setFile([bool] $pValue) { $this.file = $pValue }

    hidden [string] $filePath = ''
    [string] getFilePath() { return $this.filePath }
    [void] setFilePath([string] $pValue) { $this.filePath = $pValue }

    [List[FortahDynamicsDependency]] $dependencies = [List[FortahDynamicsDependency]]::new()
    [string] getDependencies() { return $this.dependencies }
    
    FortahDynamicsApplication() {        
    }

}