using namespace System.Collections.Generic
using namespace System.IO

using module '.\FortahDynamicsModule.psm1'

class FortahDynamicsEnvironmentBase {
    
    hidden [string] $rootPath = ''
    [string] getRootPath() { return $this.rootPath }
    [void] setRootPath([string] $pValue) { $this.rootPath = $pValue }

    FortahDynamicsEnvironmentBase([string] $pRootPath) {
        $this.rootPath = $pRootPath
    }

    [void] loadModules([List[FortahDynamicsModule]] $pModules) {
        $this.makeSureRootPathIsNotEmpty()
        [FortahDynamicsModule] $module = $null
        foreach ($module in $pModules) {
            [bool] $loaded = $false
            [string] $library = ''
            foreach ($library in $module.getLibraries()) {
                if ($this.loadLibrary($library)) {
                    $loaded = $true
                    break
                }
            }
            if ( -not ($loaded)) {
                throw "Cannot load $($module.getName()) module"
            }
        }        
    }

    hidden [void] makeSureRootPathIsNotEmpty() {
        if ($this.rootPath -eq '') {
            $this.rootPath = $this.detectRootPath()
        }        
        if ($this.rootPath -eq '') {
            throw "Cannot determine root path of Dynamics 365 Business Central"
        }
    }

    hidden [string] detectRootPath() {
        return ''        
    }

    hidden [bool] loadLibrary([string] $pLibrary) {
        [bool] $loaded = $false
        [string] $libraryPath = $this.findLibrary($pLibrary, $this.rootPath)
        if ($libraryPath -ne '') {
            $loaded = $this.loadLibraryFromPath($libraryPath)
        }
        return $loaded        
    }

    hidden [bool] loadLibraryFromPath([string] $pLibraryPath) {
        return $false
    }

    hidden [string] findLibrary([string] $pLibrary, [string] $pPath) {
        [string] $libraryPath = ''
        [List[string]] $files = [Directory]::GetFiles($pPath, $pLibrary)
        if ($files.Count -gt 0) {
            $libraryPath = $files[0]
        } else {
            [List[string]] $directories = [Directory]::GetDirectories($pPath)
            [string] $directory = ''
            foreach ($directory in $directories) {
                $libraryPath = $this.findLibrary($pLibrary, [Path]::Combine($pPath, $directory))
                if ($libraryPath -ne '') {
                    break
                }
            }
        }
        return $libraryPath
    }

}