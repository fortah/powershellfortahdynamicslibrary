using module '.\FortahDynamicsEnvironmentBase.psm1'

class FortahDynamicsStubEnvironment : FortahDynamicsEnvironmentBase {

    FortahDynamicsStubEnvironment([string] $pRootPath) : base($pRootPath) {        
    }

    hidden [string] detectRootPath() {        
        return '/home/Deevelopment/Binaries/Dynamics'
    }

    hidden [bool] loadLibraryFromPath([string] $pLibraryPath) {
        return $true
    }
}