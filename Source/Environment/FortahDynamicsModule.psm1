using namespace System.Collections.Generic

class FortahDynamicsModule {

    hidden [string] $name = ''
    [string] getName() { return $this.name }

    hidden [List[string]] $libraries = [List[string]]::new()
    [List[string]] getLibraries() { return $this.libraries }

    FortahDynamicsModule([string] $pName, [List[string]] $pLibraries) {
        $this.name = $pName
        $this.libraries = $pLibraries        
    }

}