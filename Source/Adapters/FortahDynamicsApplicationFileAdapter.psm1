using namespace System.IO
using namespace System.IO.Compression

using module '..\General\FortahDynamicsApplication.psm1'

class FortahDynamicsApplicationFileAdapter {

    hidden [string] $path = ''
    [string] getPath() { return $this.path }
    [void] setPath([string] $pValue) { $this.path = $pValue }

    FortahDynamicsApplicationFileAdapter([string] $pPath) {
        $this.path = $pPath
    }

    [FortahDynamicsApplication] read() {
        [FortahDynamicsApplication] $application = [FortahDynamicsApplication]::new()

        [FileStream] $zipStream = $null
        [ZipArchive] $zip = $null
        [ZipArchiveEntry] $zipEntry = $null
        [Stream] $zipEntryStream = $null

        try {
            $zipStream = [FileStream]::new($this.path)
            $zip = [ZipArchive]::new($zipStream)
            $zipEntry = $zip.Entries['NavManifest.xml']
            $zipEntryStream = $zipEntry.Open()
    
        } finally {
            if ($null -ne $zipEntryStream) {
                $zipEntryStream.Close()
                $zipEntryStream.Dispose()
            }
            if ($null -ne $zip) {
                $zip.Close()
                $zip.Dispose()
            }
            if ($null -ne $zipStream) {
                $zipStream.Close()
                $zipStream.Dispose()
            }
        }

        return $application
    }

}